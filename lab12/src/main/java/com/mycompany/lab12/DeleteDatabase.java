/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Siwak
 */
public class DeleteDatabase {
    public static void main(String[] args) {
        //Connection database
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        System.out.println("Connection to SQLite has been establish.");
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        String sql = "DELETE FROM category WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            
            int status = stmt.executeUpdate();
//            ResultSet key = stmt.executeUpdate(sql);
//            key.next();
//            System.out.println("" + key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
        //Close database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }
}
